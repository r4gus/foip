// https://w3c.github.io/webappsec-credential-management/#framework-credential-management
const definitions = @import("definitions.zig");

pub const create = @import("credentials/create.zig");

test "credential tests" {
    _ = create;
    _ = definitions;
}
