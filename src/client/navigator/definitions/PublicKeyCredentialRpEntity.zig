//! The PublicKeyCredentialRpEntity dictionary is used to supply additional
//! Relying Party attributes when creating a new credential.
// https://www.w3.org/TR/webauthn-2/#dictionary-rp-credential-params

/// A unique identifier for the Relying Party entity, which sets the RP ID.
///
/// This is usaually a base URL like `https://w3.org`.
id: ?[:0]const u8 = null,

/// A human-palatable name for the relying party.
///
/// For example, "ACME Corporation" or "Wonderful Widgets, Inc.".
name: [:0]const u8,
