/// This member specifies the cryptographic signature algorithm with
/// which the newly generated credential will be used, and thus also
/// the type of asymmetric key pair to be generated, e.g., RSA or Elliptic Curve.
alg: @import("zbor").cose.Algorithm,
/// This member specifies the type of credential to be created. The value SHOULD be
/// a member of PublicKeyCredentialType but client platforms MUST ignore unknown
/// values, ignoring any PublicKeyCredentialParameters with an unknown type.
type: [:0]const u8,
