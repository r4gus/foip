//! This dictionary contains the attributes that are specified by a caller when referring
//! to a public key credential as an input parameter to the create() or get() methods.
//! It mirrors the fields of the PublicKeyCredential object returned by the latter methods.
// https://www.w3.org/TR/webauthn-2/#dictionary-credential-descriptor

/// This member contains the credential ID of the public key credential the caller is referring to.
id: []const u8,
/// This member contains the type of the public key credential the caller is referring to.
/// The value SHOULD be a member of PublicKeyCredentialType but client platforms MUST ignore
/// any PublicKeyCredentialDescriptor with an unknown type.
type: [:0]const u8,
/// This OPTIONAL member contains a hint as to how the client might communicate with the
/// managing authenticator of the public key credential the caller is referring to.
transports: ?[][:0]const u8 = null,
