//! WebAuthn Relying Parties may use the AuthenticatorSelectionCriteria dictionary to
//! specify their requirements regarding authenticator attributes.

/// If this member is present, eligible authenticators are filtered to only
/// authenticators attached with the specified § 5.4.5 Authenticator Attachment
/// Enumeration (enum AuthenticatorAttachment).
///
/// Should be "platform" or "cross-platform".
authenticatorAttachment: ?[:0]const u8,
/// Specifies the extent to which the Relying Party desires to create a
/// client-side discoverable credential.
///
/// Should be "discouraged", "preferred" or "required".
residentKey: ?[:0]const u8,
/// This member is retained for backwards compatibility with WebAuthn Level 1
/// and, for historical reasons.
requireResidentKey: bool = false,
/// This member describes the Relying Party's requirements regarding user
/// verification for the create() operation. Eligible authenticators are
/// filtered to only those capable of satisfying this requirement.
userVerification: [:0]const u8 = "preferred",
