// https://www.w3.org/TR/webauthn-2/#dictionary-makecredentialoptions

const PublicKeyCredentialRpEntity = @import("PublicKeyCredentialUserEntity.zig");
const PublicKeyCredentialUserEntity = @import("PublicKeyCredentialUserEntity.zig");
const PublicKeyCredentialParameters = @import("PublicKeyCredentialParameters.zig");
const PublicKeyCredentialDescriptor = @import("PublicKeyCredentialDescriptor.zig");
const AuthenticatorSelectionCriteria = @import("AuthenticatorSelectionCriteria.zig");

/// This member contains data about the Relying Party responsible for the request.
rp: PublicKeyCredentialRpEntity,
/// This member contains data about the user account for which the Relying Party
/// is requesting attestation.
user: PublicKeyCredentialUserEntity,
/// This member contains a challenge intended to be used for generating the newly
/// created credential’s attestation object.
challenge: []const u8,
/// This member contains information about the desired properties of the credential
/// to be created. The sequence is ordered from most preferred to least preferred.
pubKeyCredParams: []const PublicKeyCredentialParameters,
/// This member specifies a time, in milliseconds, that the caller is willing to
/// wait for the call to complete.
timeout: u64 = 20000, // 20s default
/// This member is intended for use by Relying Parties that wish to limit the
/// creation of multiple credentials for the same account on a single authenticator.
excludeCredentials: []const PublicKeyCredentialDescriptor = &.{},
/// This member is intended for use by Relying Parties that wish to select the
/// appropriate authenticators to participate in the create() operation.
authenticatorSelection: ?AuthenticatorSelectionCriteria,
/// This member is intended for use by Relying Parties that wish to express their
/// preference for attestation conveyance.
///
/// Should be "none", "indirect", "direct", "enterprise".
attestation: [:0]const u8 = "none",
// TODO: extensions
