//! The PublicKeyCredentialUserEntity dictionary is used to supply additional
//! user account attributes when creating a new credential.
// https://www.w3.org/TR/webauthn-2/#dictionary-user-credential-params

/// The user handle of the user account entity.
///
/// A user handle is an opaque byte sequence with a maximum size of 64 bytes,
/// and is not meant to be displayed to the user.
id: [64]u8,

/// A human-palatable name for the user.
///
/// It aids the user in determining the difference between user accounts
/// with similar displayNames.
name: [:0]const u8,

/// A human-palatable name for the user account, intended only for display.
displayName: [:0]const u8,
