// https://www.w3.org/TR/webauthn-2/#sctn-credentialcreationoptions-extension

const PublicKeyCredentialCreationOptions = @import("PublicKeyCredentialCreationOptions.zig");

publicKey: PublicKeyCredentialCreationOptions,
