# FOIP - FIDO over IP

The W3C [WebAuthn](https://www.w3.org/TR/webauthn-2/#sctn-authenticator-attachment-modality) 
sepcification describes a platfrom authenticator as:

> A platform authenticator is attached using a client device-specific transport, 
> called platform attachment, and is usually not removable from the client device. 
> A public key credential bound to a platform authenticator is called a platform credential.

The issue is that nobody has defined (yet) how a device-specific transport should look like.
Some applications that support the FIDO client API (e.g. Firefox) have code specifically
written to communication with services like Windows Hello or Apple ID, but there
is no general solution available. A user should be able to select from a vast pool
of platfrom authenticator solutions the one she likes the most, similar to how
one can change his default browser or password manager.

We propose FIDO over IP, a local server that listens on port $F1D0_{16} = 61904$.
The server runs an application that implements the [CTAP2](https://fidoalliance.org/specs/fido-v2.1-ps-20210615/fido-client-to-authenticator-protocol-v2.1-ps-errata-20220621.html) protocol.
This eliminates device specific IPC.

For this to work two steps are required:

1. Develop a library that can be used in client applications to add FIDO2 authentication.
** This library mimics the [MDN API](https://developer.mozilla.org/en-US/docs/Web/API/Web_Authentication_API)
** The library should support transport specific bindings like USB, NFC, BLE aswell as communication with a server over port $FID0_{16}$
2. Develop a (local) server test application that runs an platform authenticator.


